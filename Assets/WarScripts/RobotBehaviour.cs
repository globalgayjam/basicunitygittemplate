﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotBehaviour : MonoBehaviour
{

    public string robotName = "";
    public List<string> takesPainFrom = new List<string>();

    public float Force = 0.22f;
    public float Health = 100f;
    public float Damage = 10f;
    public float FireRate = 0.1f;

    public float AttackRange = 3f;

    [Header("1 range - 2 melee")]
    public int AttackAnimation = 2;

  //  [Header("1 range - 2 melee")]
  //  public int DamageAnimation = 2;

    public GameObject Drop;

    private float MinDistanceToRobots = 1f;

    private Rigidbody _rigid;

    private float _laneDepth;
    private int _lane;
    private WarManager.Side _side;

    private LayerMask _enemyLayer;

    private float _LastAttack = 0f;

    private bool _changingLane = false;

    public Animator RobotAnimator;

    private float maxhealth;

    public ParticleSystem DamageParticles;

    private bool _isAttacking = false;

    public GameObject HitParticles;

    public GameObject DeathParticles;

    public GameObject ShootParticles;

    [FMODUnity.EventRef]
    public string ExplosionAudio;
    [FMODUnity.EventRef]
    public string MovingAudio;
    [FMODUnity.EventRef]
    public string AttackAudio;
    private FMOD.Studio.EventInstance audioInstanceMoving;
    private FMOD.Studio.EventInstance audioInstanceAttack;
    public void SetLane(int lane) {
        _lane = lane;
        _laneDepth = WarManager.Instance.Lanes[lane].ZDepth;
    //    Debug.Log(_laneDepth + " -- " + transform.position.z);
    }

    public void SetSide(WarManager.Side side)
    {
        _side = side;

        if (side == WarManager.Side.Left)
        {
            gameObject.layer = 9;
            _enemyLayer = LayerMask.GetMask("RobotsRight");
        }

        if (side == WarManager.Side.Right)
        {
            gameObject.layer = 8;
            _enemyLayer = LayerMask.GetMask("RobotsLeft");
        }
    }

    // Start is called before the first frame update
    void Start()
    {


        maxhealth = Health;
        RobotAnimator.SetBool("move", true);
        audioInstanceMoving.start();
        _rigid = GetComponent<Rigidbody>();


        //robots get faster when time goes on
        float speed = 1f +WarManager.Instance.Speedbonus;
        Force *= speed;
        FireRate /= speed;
    }

    void OnCollisionEnter(Collision collision) {
        HandleCollision(collision);
    }

    void OnCollisionStay(Collision collision)
    {
        HandleCollision(collision);
    }

    private void HandleCollision(Collision collision)
    {
        if (_changingLane)
        {
            return;
        }

        if (collision.gameObject.tag == "Player")
        {

            float input = collision.gameObject.GetComponent<PlayerBehaviour>().GetVerticalInput();

            if (collision.transform.position.z < transform.position.z && input > 0.5f && _lane < 4)
            {
                //no room to move
                if (CheckRaycast(1f))
                {
                    return;
                }
                SetLane(_lane + 1);
            }

            if (collision.transform.position.z > transform.position.z && input < -0.5f && _lane > 0)
            {
                //no room to move
                if (CheckRaycast(-1f))
                {
                    return;
                }
                SetLane(_lane - 1);
            }

        }
    }

    private bool CheckRaycast(float dir) {

        RaycastHit hit;
        RaycastHit hit2;
        RaycastHit hit3;
  
        Physics.Raycast(transform.position + transform.right * 0.5f, Vector3.forward * dir, out hit, 2f);
        Physics.Raycast(transform.position + -transform.right * 0.5f,Vector3.forward * dir, out hit2, 2f);
        Physics.Raycast(transform.position, Vector3.forward * dir, out hit3, 2f);

        //if casts hit something, return true
        return hit.collider != null || hit2.collider != null || hit3.collider != null;


    }

    void Update()
    {

        if (Health < 0)
        {
            return;
        }

        if (Mathf.Abs(transform.position.x) > 13f)
        {
            Destroy(gameObject);
        }

        RaycastHit hit;
        Physics.Raycast(transform.position, transform.right, out hit, AttackRange, _enemyLayer);


        if (Time.time > _LastAttack + 0.33f)
        {
            //stop attacking after while of not attacking
            RobotAnimator.SetBool("move", true);
            audioInstanceMoving.start();
            RobotAnimator.SetBool("attack", false);
        }

        //check attacking
        if (hit.collider == null)
        {
            _isAttacking = false;
            return;
        }

        _isAttacking = true;

        if (Time.time < _LastAttack + FireRate)
        {
            return;
        }
        
    

        RobotBehaviour enemy = hit.collider.gameObject.GetComponent<RobotBehaviour>();

        if (enemy == null)
        {
            return;
        }

        //TODO trigger attack animation
        RobotAnimator.SetBool("move", false);
        audioInstanceMoving.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        RobotAnimator.SetBool("attack", true);
        RobotAnimator.SetInteger("attack num", AttackAnimation == 1 ? 1 : Random.Range(2,5));

        if (HitParticles != null)
        {
            Instantiate(HitParticles, hit.point, Quaternion.identity);

        }

        if (ShootParticles != null)
        {
            Instantiate(ShootParticles, transform.position, transform.rotation);

        }

        _LastAttack = Time.time;
        FMODUnity.RuntimeManager.PlayOneShot(AttackAudio);
        enemy.DealDamage(Damage, robotName);

    }

    public void DealDamage(float damage, string  source)
    {

        if (Health < 0)
        {
            return;
        }

        //range saa olla vielä tehokkaampi meleeseen
        if (source == "range" && robotName == "melee")
        {
            damage *= 1.77f;
        }

        //melee  saa tehä vielä lisää tankkeihin
        if (source == "melee" && robotName == "tank")
        {
            damage *= 1.5f;
        }

       //meleet ja ranget ei oo kauheen tehokkaita toisiinsa
        if (source == "melee" && robotName == "melee")
        {
            damage *= 0.066f;
        }

        if (source == "range" && robotName == "range")
        {
            damage *= 0.066f;
        }


        Health -= damage;

        if (takesPainFrom.Contains(source))
        {
            RobotAnimator.SetBool("damage", true);
            RobotAnimator.SetInteger("damage num", 1);
        }


        if (Health < maxhealth / 2 && !DamageParticles.isPlaying)
        {
            DamageParticles.Play();
        }

        if (Health <= 0)
        {
     
            Instantiate(Drop, transform.position + new Vector3(Random.Range(-0.33f, 0.33f), 0.25f,Random.Range(-0.33f, 0.33f)) , Quaternion.identity);

            Instantiate(WarManager.Instance.OilBarrel, transform.position + new Vector3(Random.Range(-0.33f, 0.33f), 0.25f, Random.Range(-0.33f, 0.33f)), Quaternion.identity);


            StartCoroutine(Die());
            // Destroy(gameObject);
        }

    }

    private IEnumerator Die()
    {
        RobotAnimator.SetBool("damage", true);
        RobotAnimator.SetInteger("damage num", 1);

        yield return new WaitForSeconds(0.33f);

        if (DeathParticles != null)
        {
            Instantiate(DeathParticles, transform.position, Quaternion.identity);

        }

        Destroy(gameObject);

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Health < 0)
        {
            return;
        }

        if (_changingLane)
        {
            return;
        }

        //jump lane
        if (Mathf.Abs(transform.position.z - _laneDepth) > 0.5f && !_changingLane) {
    
         //   transform.position = new Vector3(transform.position.x, 0f, _laneDepth);
            StartCoroutine(ChangeLane(new Vector3(transform.position.x, transform.position.y, _laneDepth)));

        }

        RaycastHit hit;
        Physics.Raycast(transform.position, transform.right, out hit, MinDistanceToRobots);

        if (hit.collider == null && !_isAttacking)
        {
            _rigid.AddForce(transform.right * Time.deltaTime * Force);
        }
          

    }

    private IEnumerator ChangeLane(Vector3 target)
    {

        _changingLane = true;

        bool done = false;
        float speed = 12f;

        while (!done)
        {

            Vector3 distance = target - transform.position;
            Vector3 move = distance.normalized * Time.deltaTime * speed;

            if (move.magnitude > distance.magnitude)
            {
                done = true;
            }
            else
            {
                transform.position = transform.position + move;
            }

            yield return null;
        }
        transform.position = target;
        _changingLane = false;

    }


}
