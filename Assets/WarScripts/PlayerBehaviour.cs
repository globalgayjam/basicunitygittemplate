﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    public float Force = 1000f;

    private Rigidbody _rigid;

    private  float xInput;
    private  float zINput;

    public float _items = 0f;

    public Animator animator;
    public GameObject model;

    public List<DroppableItem> _collectedItems = new List<DroppableItem>();

     public float GetItemTrainDistance(DroppableItem item)
     {

         _collectedItems.Add(item);
         _items += 1f;
         return _items;

     }

    // Start is called before the first frame update
    void Start()
    {

        _rigid = GetComponent<Rigidbody>();
        
    }

    public float GetVerticalInput() {
        return -zINput;
    }


    void Update()
    {

        if (_rigid.velocity.magnitude > 0.5f)
        {
            animator.SetBool("move", true);
            animator.speed = 2f;

            Vector3 v3 = _rigid.velocity;

            Vector2 v = new Vector2(v3.x, -v3.z);

            float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;

            model.transform.rotation = Quaternion.AngleAxis(angle + 90f, Vector3.up);
            
        }
        else
        {
            animator.SetBool("move", false);
            animator.speed = 1f;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {

       xInput = Input.GetAxisRaw("Horizontal");
       zINput = Input.GetAxisRaw("Vertical"); 

       _rigid.AddForce(new Vector3(-xInput, 0, -zINput) * Time.deltaTime * Force);
        
    }
}
