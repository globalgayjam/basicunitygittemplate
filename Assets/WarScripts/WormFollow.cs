﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormFollow : MonoBehaviour
{

    public Transform Target;

    public float Distance = 1f;

    private List<Vector3> positions = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        positions.Add(Target.position);

        int last = positions.Count -1;

        //find position

        float dis = Vector3.Distance(Target.position, positions[last]);

        for (int n = last -1; n >= 0; n--)
        {
            dis += Vector3.Distance(positions[n +1], positions[n]);

            if (dis > Distance)
            {
                transform.position = positions[n];
                return;
            }
        }

        transform.position = positions[0];

    }
}
