﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarManager : MonoBehaviour
{

    public List<RobotData> Robots = new List<RobotData>();
    public List<LaneData> Lanes = new List<LaneData>();

    private List<GameObject> rightRobots = new List<GameObject>();
    private List<GameObject> leftRobots = new List<GameObject>();

    public float Speedbonus = 0f;
    public float SecondsToIncreaseSpeedBoost = 40f;
    public float SpeedbonusIncrease = 0.1f;

    public GameObject OilBarrel;

    private List<int> _lastRobots = new List<int>();

    [System.Serializable]
    public struct RobotData {

        public string name;
        public GameObject Prefab;
    //    public GameObject Drop;
    }

    [System.Serializable]
    public class LaneData {

        [Header("dont change")]
        public float ZDepth;

        public Transform RightSpawnPosition;
        public Transform LeftSpawnPosition;
    }


    public static WarManager Instance;

    public float SpawnInterval = 3f;

    private float _lastSpawn = -99f;

    // Start is called before the first frame update
    void Start()
    {

        for (int n = 0; n < Lanes.Count; n++ )
        {
            Lanes[n].ZDepth = Lanes[n].RightSpawnPosition.position.z;
        }
        
        if (Instance == null) {
            Instance = this;
        }

    }

    void OnEnable()
    {


        StopAllCoroutines();
        StartCoroutine(Increase());

    }

    private IEnumerator Increase()
    {
        while(true) {

            yield return new WaitForSeconds(SecondsToIncreaseSpeedBoost);

            Speedbonus += SpeedbonusIncrease;
        }        

    }

       

    // Update is called once per frame
    void Update()
    {

        int leftn = 0;
        foreach (var robot in leftRobots)
        {
            if (robot != null)
            {
                leftn++;           
            }
        }
        int rightn = 0;
        foreach (var robot in rightRobots)
        {
            if (robot != null)
            {
                rightn++;
            }
        }

        if (Time.time > _lastSpawn + SpawnInterval)
        {
            _lastSpawn = Time.time;
            if (SpawnInterval > 0.5f) {
                SpawnInterval -= 0.1f;
            }
            SpawnRobot(Side.Left);
            SpawnRobot(Side.Right);

            return;
        }
        
        //if there is no robots on one side spawn
        if (leftn == 0)
        {
            SpawnRobot(Side.Left);
        }
        if (rightn == 0)
        {
            SpawnRobot(Side.Right);
        }

    }


    private void SpawnRobot(Side side) {

        List<int> temp = new List<int>();

        int o = 0;
        for(int n = _lastRobots.Count -1; n > 0; n--) {
            temp.Add(_lastRobots[n]);
            o++;
            if (o > 4) {
                break;
            }
        }
       

        int robotNumber = (int)Random.Range(0, Robots.Count);


        if (!temp.Contains(1)) {
            robotNumber = 1;
        }

        if (!temp.Contains(2)) {
            robotNumber = 2;
        }

        if (!temp.Contains(0))
        {
            robotNumber = 0;
        }

        GameObject robot = (GameObject)Instantiate(Robots[robotNumber].Prefab);

        int laneNumber = (int)Random.Range(0, Lanes.Count);

        if (side == Side.Left) {
            robot.transform.position = Lanes[laneNumber].LeftSpawnPosition.position;
            leftRobots.Add(robot);

        }
        else
        {
            robot.transform.position = Lanes[laneNumber].RightSpawnPosition.position;
            robot.transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
            rightRobots.Add(robot);
        }

        robot.GetComponent<RobotBehaviour>().SetLane(laneNumber);
        robot.GetComponent<RobotBehaviour>().SetSide(side);

        _lastRobots.Add(robotNumber);


    }

    public enum Side {
        Left,
        Right,
    }
}
