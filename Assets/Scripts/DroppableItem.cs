﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppableItem : MonoBehaviour
{
    public int[] DroppedItems = new int[4];
    public GameObject myModel;
    private bool scaling;
    private float scale;
    public AnimationCurve scaleCurve;
    [FMODUnity.EventRef]
    public string PickUpAudio;
    [FMODUnity.EventRef]
    public string DropAudio;
    private bool _collected = false;

    // Start is called before the first frame update
    private void OnEnable() {
        FMODUnity.RuntimeManager.PlayOneShot(DropAudio);
        scaling = true;
        scale = 0;
        Invoke("goingToDestroy", 15f);
    }
    private void OnDisable() {
        Destroy(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        if (scaling) {
            if(scale <1) scale += Time.deltaTime;
            myModel.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, scaleCurve.Evaluate(scale / 1));
        }
    }

    private void OnTriggerEnter(Collider other) {

        if (_collected)
        {
            return;
        }

        if (other.tag == "Player") {
            if (OrderManager.instance.GatherItem(DroppedItems)) {
                FMODUnity.RuntimeManager.PlayOneShot(PickUpAudio);
                CancelInvoke("goingToDestroy");
                float trainDistance = other.gameObject.GetComponent<PlayerBehaviour>().GetItemTrainDistance(this);

                WormFollow follow = gameObject.AddComponent<WormFollow>();

                follow.Distance = trainDistance;
                follow.Target = other.transform;

                _collected = true;
            }
        }
    }
    void goingToDestroy() {
        Destroy(this.gameObject);
    }
}
