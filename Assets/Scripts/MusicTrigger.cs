﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTrigger : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string AudioOutside;
    [FMODUnity.EventRef]
    public string AudioInside;
    private FMOD.Studio.EventInstance audioInstanceInside;
    private FMOD.Studio.EventInstance audioInstanceOutside;
    private FMOD.Studio.PARAMETER_ID FMODPID;
    private void Start() {
        audioInstanceOutside = FMODUnity.RuntimeManager.CreateInstance(AudioOutside);
        audioInstanceInside = FMODUnity.RuntimeManager.CreateInstance(AudioInside);
        audioInstanceInside.start();
        audioInstanceOutside.start();
        audioInstanceOutside.setVolume(0);
        //FMOD.Studio.EventDescription musicEventDescription;
        //audioInstanceInside.getDescription(out musicEventDescription);
        //FMOD.Studio.PARAMETER_DESCRIPTION musicParameterDescription;
        //musicEventDescription.getParameterDescriptionByName("outside", out musicParameterDescription);
        //FMODPID = musicParameterDescription.id;
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other) {
        if (other.tag != "Player") return;
        audioInstanceInside.setVolume(1);
        audioInstanceOutside.setVolume(0);
        //audioInstanceInside.setParameterByID(FMODPID, 1);

    }
    private void OnTriggerExit(Collider other) {
        if (other.tag != "Player") return;
        audioInstanceOutside.setVolume(1);
        audioInstanceInside.setVolume(0);
        //audioInstanceInside.setParameterByID(FMODPID, 0);
    }
}
