﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;

public class OrderManager : MonoBehaviour {

    public static OrderManager instance;
    [HideInInspector]
    public int[] currentOrderItems;
    [HideInInspector]
    public int[] currentGatheredItems = new int[4];
    [HideInInspector]
    public OrderPlayerInterface playerOrderPos;
    public float StartGameTime = 180f;
    float gameTime;
    public TextMeshProUGUI GameTimer;
    [HideInInspector]
    public bool doingOrder;
    [FMODUnity.EventRef]
    public string CompletedOrder;
    [FMODUnity.EventRef]
    public string playerTookOrder;
    [FMODUnity.EventRef]
    public string TimeRunningOut;
    private FMOD.Studio.EventInstance audioInstanceTimeWarning;
    // Start is called before the first frame update
    public PlayerBehaviour Player;
    private void Awake() {
        if (!instance)
            instance = this;
        else Destroy(this);
        playerOrderPos = FindObjectOfType<OrderPlayerInterface>();
        gameTime = StartGameTime;
        if (TimeRunningOut != "")
        audioInstanceTimeWarning = FMODUnity.RuntimeManager.CreateInstance(TimeRunningOut);
    }
    bool warningOn;
    // Update is called once per frame
    void Update() {
        gameTime -= Time.deltaTime;
        if (gameTime<=20f && !warningOn) {
            warningOn = true;
            audioInstanceTimeWarning.start();
        } else if (gameTime>15f && warningOn) {
            warningOn = false;
            audioInstanceTimeWarning.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
        if (gameTime <= 0) {
            SceneManager.LoadScene("MainMenu");
        }
        string minutes = Mathf.Floor(gameTime / 60).ToString("00");
        string seconds = (gameTime % 60).ToString("00");
        GameTimer.text = string.Format("{0}:{1}", minutes, seconds);
    }



    public void StartOrder(int[] newOrder) {
        if (!Player) {
            Player = FindObjectOfType<PlayerBehaviour>();
        }
        doingOrder = true;
        currentOrderItems = new int[4];
        for (int i = 0; i < newOrder.Length; i++) {
            currentOrderItems[i] = newOrder[i];
        }
        playerOrderPos.StartOrder(newOrder);
        //start timer, set current order
        FMODUnity.RuntimeManager.PlayOneShot(playerTookOrder);
    }
    private bool gatheredItem;
    public bool GatherItem(int[] items) {
        //check if current order is completed
        if (!doingOrder) return false;
        gatheredItem = false;
        for (int i = 0; i < items.Length; i++) {
            if (items[i] > 0) {
                if (currentGatheredItems[i] < currentOrderItems[i]) {
                    currentGatheredItems[i] += items[i];
                    gatheredItem = true;
                }
            }
        }
        playerOrderPos.UpdateGathered(items);
        if (gatheredItem) return true;
        else return false;

    }

    public bool CheckCurrentOrder() {
        if (currentGatheredItems.SequenceEqual(currentOrderItems)) {
            ClearFollowingItems();
            doingOrder = false;
            FMODUnity.RuntimeManager.PlayOneShot(CompletedOrder);
            StartGameTime += 30f;
            gameTime += 30f;
            currentGatheredItems = new int[4];
            return true;
        } else {
            return false;
        }
    }
    public void ClearFollowingItems() {
        foreach (var item in Player._collectedItems) {
            Destroy(item.gameObject);
        }
        Player._collectedItems.Clear();
        Player._items = 0;
    }
}
