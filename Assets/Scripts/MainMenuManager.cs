﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject MainMenuPanel;
    public GameObject HowToPlayPanel;
    public GameObject CreditsPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame() {
        SceneManager.LoadScene("scene_repairshop");
        SceneManager.LoadScene("scene_repairshop_logic", LoadSceneMode.Additive);
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void Back() { }
    public void Credits() { }
    public void HowTo() { }
}
