﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{
    public List<Transform> CustomerStarts;
    public List<GameObject> Customers;
    private float spawnTimer;
    private float difficultyTimer;
    public float DifficultyTime;
    public float SpawnTime;
    int nonActive;
    public int CurrentDifficulty;
    // Start is called before the first frame update
    void Start()
    {
        difficultyTimer = 0;
        CurrentDifficulty = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (nonActive < 0) {
            for (int i = 0; i < Customers.Count; i++) {
                if (!Customers[i].activeInHierarchy) {
                    nonActive = i;
                    break;
                } else {
                    nonActive = -1;
                }
            }
        } else {
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= SpawnTime) {
                SpawnCustomer();
            }
        }
        difficultyTimer += Time.deltaTime;
        if (difficultyTimer>= DifficultyTime) {
            CurrentDifficulty++;
            difficultyTimer = 0;
        }
    }
    void SpawnCustomer() {
        Customers[nonActive].SetActive(true);
        Customers[nonActive].GetComponent<Customer>().GenerateOrder(CurrentDifficulty);
        nonActive = -1;
        spawnTimer = 0f;

    }
}
