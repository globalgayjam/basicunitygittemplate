﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OrderPlayerInterface : MonoBehaviour
{
    public Image ImageLayoutBackground;
    public int[] gatheredOrder = new int[4];
    public GameObject[] Items = new GameObject[4];
    // Start is called before the first frame update
    public Image OrderMissed;
    void Start()
    {
        OrderMissed.enabled = false;
    }
    private int itemsLeft;
    public void StartOrder(int[] newOrder) {
        OrderMissed.enabled = false;
        gatheredOrder = newOrder;
        ImageLayoutBackground.gameObject.SetActive(true);
        for (int x = 0; x < gatheredOrder.Length; x++) {
            if (gatheredOrder[x] > 0) {
                Items[x].SetActive(true);
                Items[x].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + gatheredOrder[x];
            } else {
                Items[x].SetActive(false);
            }
        }
    }
    public void UpdateGathered(int[] items) {
        itemsLeft = 0;
        for (int x = 0; x < gatheredOrder.Length; x++) {
            if (items[x] > 0 && gatheredOrder[x]>0) {
                gatheredOrder[x] -= items[x];
                Items[x].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + (gatheredOrder[x]);

            }
            itemsLeft += gatheredOrder[x];
            if (gatheredOrder[x] <= 0) {
                Items[x].SetActive(false);
            }
            
        }
        if (itemsLeft <= 0) {
            ImageLayoutBackground.gameObject.SetActive(false);
        }
    }
    public void ClearOrder() {
        OrderMissed.enabled = true;
        gatheredOrder = new int[4];
        for (int x = 0; x < Items.Length; x++) {
            Items[x].SetActive(false);
        }
        itemsLeft = 0;
        ImageLayoutBackground.gameObject.SetActive(false);
    }
}
