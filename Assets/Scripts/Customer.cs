﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Customer : MonoBehaviour
{

    public int[] MyOrder;
    public float MyWaitingTime;
    public float currentTime;
    public Transform myWaypoint;
    private Vector3 startPoint;
    private float walkTime;
    public float moveSpeed;
    private bool leaving;
    private bool waiting;
    private Vector3 startRotation;
    public AnimationCurve rotationCurve;
    public Image ImageLayoutBackground;
    public Image OrderTaken;
    public GameObject[] Items = new GameObject[4];
    public GameObject[] myModels;
    [FMODUnity.EventRef]
    public string playerTookOrder;
    [FMODUnity.EventRef]
    public string playerMissedOrder;
    [FMODUnity.EventRef]
    public string CustomerEntered;
    [FMODUnity.EventRef]
    public string[] CustomerTalk;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        startRotation = transform.eulerAngles;
    }
    private void OnEnable() {
        for (int i = 0; i < myModels.Length; i++) {
            myModels[i].SetActive(false);
        }
        int randomModel = Random.Range(0, myModels.Length);
        myModels[randomModel].SetActive(true);
        transform.eulerAngles = startRotation;
        startPoint = transform.position;
        lerpRotation = 0;
        currentTime = 0;
        walkTime = 0;
        leaving = false;
        OrderTaken.enabled = false;
        anim = myModels[randomModel].GetComponent<Animator>();
        FMODUnity.RuntimeManager.PlayOneShot(CustomerEntered);
        anim.SetBool("move", true);
        //GenerateOrder(2); testing purposes
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject && waiting) {
            ImageLayoutBackground.gameObject.SetActive(true);
            currentTime += Time.deltaTime;
            ImageLayoutBackground.fillAmount = (MyWaitingTime - currentTime)/MyWaitingTime;
            if (startedOrder) {
                OrderManager.instance.playerOrderPos.ImageLayoutBackground.fillAmount = (MyWaitingTime - currentTime) / MyWaitingTime;
            }
        }
        if (currentTime >= MyWaitingTime) {
            WaitTimeReached();
        }
        if (leaving && lerpRotation<1f) {
            lerpRotation += Time.deltaTime;
        }
        if (walkTime < 1 && !leaving) {
            walkTime += Time.deltaTime * moveSpeed;
        } else if (walkTime>0f && leaving && lerpRotation>=1) {
            walkTime -= Time.deltaTime * moveSpeed;
        } else if (walkTime >= 1 && !leaving) {
            waiting = true;
            anim.SetBool("move", false);
        }
        transform.position = Vector3.Lerp(startPoint, myWaypoint.position, walkTime);
        if(waiting && Random.Range(0f,100f)<1f)
        FMODUnity.RuntimeManager.PlayOneShot(CustomerTalk[Random.Range(0,CustomerTalk.Length)]);
    }
    
    public void GenerateOrder(int difficulty) {
        MyOrder = new int[4];
        for (int i = 0; i < difficulty; i++) {
            MyOrder[Random.Range(0, 4)]++;
        }
        ImageLayoutBackground.gameObject.SetActive(true);
        for (int x = 0; x < MyOrder.Length; x++) {
            if (MyOrder[x] > 0) {
                Items[x].SetActive(true);
                Items[x].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + MyOrder[x];
            } else {
                Items[x].SetActive(false);
            }
        }
        ImageLayoutBackground.gameObject.SetActive(false);
    }
    private float lerpRotation;
    private bool startedOrder;
    public void StartOrder() {
        if (!OrderManager.instance.doingOrder) {
            OrderManager.instance.StartOrder(MyOrder);
            currentTime = 0f;
            startedOrder = true;
            OrderTaken.enabled = true;
            FMODUnity.RuntimeManager.PlayOneShot(playerTookOrder);
        }
    }
    bool playedOnce;
    private void WaitTimeReached() {
        //leave the scene
        ImageLayoutBackground.gameObject.SetActive(false);
        transform.eulerAngles = new Vector3(transform.rotation.x, Mathf.Lerp(startRotation.y, startRotation.y + 180, rotationCurve.Evaluate(lerpRotation/1)));
        leaving = true;
        waiting = false;
        if (startedOrder) {
            if (!playedOnce) {
                FMODUnity.RuntimeManager.PlayOneShot(playerMissedOrder);
                playedOnce = true;
            }
            OrderManager.instance.doingOrder = false;
            OrderManager.instance.currentGatheredItems = new int[4];
            OrderManager.instance.playerOrderPos.ClearOrder();
            OrderManager.instance.ClearFollowingItems();
        }
        Invoke("SetActivity", 5);
    }
    private void SetActivity() {
        startedOrder = false;
        playedOnce = false;
        this.gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Player" && !leaving) {
            if (Input.GetKey(KeyCode.Space)) {
                if (!startedOrder) {
                    StartOrder();
                } else {
                    if (OrderManager.instance.CheckCurrentOrder()) {
                        OrderTaken.enabled = false;
                        ImageLayoutBackground.gameObject.SetActive(false);
                        transform.eulerAngles = new Vector3(transform.rotation.x, Mathf.Lerp(startRotation.y, startRotation.y + 180, rotationCurve.Evaluate(lerpRotation / 1)));
                        leaving = true;
                        waiting = false;
                        Invoke("SetActivity", 5);
                    }
                }
            }
        }
    }
}
