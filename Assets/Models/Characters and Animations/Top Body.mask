%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Top Body
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Hips
    m_Weight: 1
  - m_Path: Armature/Hips/Body
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.L.1
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.L.1/Arm.L.2
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.L.1/Arm.L.2/Arm.L.2_end
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.R.1
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.R.1/Arm.R.2
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Arm.R.1/Arm.R.2/Arm.R.2_end
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Head
    m_Weight: 1
  - m_Path: Armature/Hips/Body/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Hips/Leg.L.1
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.L.1/Leg.L.2
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.L.1/Leg.L.2/Feet.L
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.L.1/Leg.L.2/Feet.L/Feet.L_end
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.R.1
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.R.1/Leg.R.2
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.R.1/Leg.R.2/Feet.R
    m_Weight: 0
  - m_Path: Armature/Hips/Leg.R.1/Leg.R.2/Feet.R/Feet.R_end
    m_Weight: 0
  - m_Path: Armature/Hips/Wheel.L
    m_Weight: 0
  - m_Path: Armature/Hips/Wheel.L/Wheel.L_end
    m_Weight: 0
  - m_Path: Armature/Hips/Wheel.R
    m_Weight: 0
  - m_Path: Armature/Hips/Wheel.R/Wheel.R_end
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 0
  - m_Path: Armature/Root/Root_end
    m_Weight: 1
  - m_Path: Arms_1
    m_Weight: 1
  - m_Path: Arms_2
    m_Weight: 1
  - m_Path: Arms_3
    m_Weight: 1
  - m_Path: Body_1
    m_Weight: 1
  - m_Path: Body_2
    m_Weight: 1
  - m_Path: Body_3
    m_Weight: 1
  - m_Path: Head_1
    m_Weight: 1
  - m_Path: Head_2
    m_Weight: 1
  - m_Path: Head_3
    m_Weight: 1
  - m_Path: Legs_1
    m_Weight: 1
  - m_Path: Legs_2
    m_Weight: 1
  - m_Path: Legs_3
    m_Weight: 1
