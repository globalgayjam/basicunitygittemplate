﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwapper : MonoBehaviour
{
    public GameObject Camera;
    public GameObject OtherCamera;
    public GameObject WarManager;
    public bool outside;

    // Start is called before the first frame update

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            OtherCamera.SetActive(false);
            Camera.SetActive(true);
            if (outside) WarManager.SetActive(true);
            else WarManager.SetActive(false);
        }
    }
}
